#version 330

uniform mat4 V;
uniform mat4 M;
uniform vec4 lp;
uniform sampler2D textureMap0;
uniform sampler2D textureMap1;

in vec4 i_c;
in vec4 i_v;
in vec4 i_n;
in vec2 i_TexCoord0;

out vec4 pixelColor; //Zmienna wyjsciowa fragment shadera. Zapisuje sie do niej ostateczny (prawie) kolor piksela


void main(void) {

	//vec4 lp=vec4(0,0,-6,1); //wsp. �wiat�a w przestrzeni �wiata
	//swiata=M*modelu
	//oka=V*swiata
	//przyciecia=P*oka

	vec4 l=normalize(V*lp-V*M*i_v); //oka
	vec4 n=normalize(V*M*i_n); //oka

	float nl=clamp(dot(n,l),0,1);

	vec4 r=reflect(-l,n); //oka
	vec4 v=normalize(vec4(0,0,0,1)-V*M*i_v);

	float rv=clamp(dot(r,v),0,1);
	rv=pow(rv,25);

	vec4 texColor=texture(textureMap0,i_TexCoord0);
	vec4 texColor2=texture(textureMap1,((n.xy+1)/2));

	vec4 ka=texColor;
	vec4 kd=texColor*0.6+texColor2*0.4;
	vec4 ks=vec4(1,1,1,1);
	vec4 la=vec4(0,0,0,1);
	vec4 ld=vec4(1,1,1,1);
	vec4 ls=vec4(1,1,1,1);

	pixelColor=ka*la+kd*ld*nl+ks*ls*rv;
}
