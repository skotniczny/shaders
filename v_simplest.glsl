#version 330



//Zmienne jednorodne
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;


//Atrybuty
in vec4 vertex; //wspolrzedne wierzcholka w przestrzeni modelu
in vec4 color;
in vec4 normal; //wektor normalny w przestrzeni modelu

in vec2 texCoord0;

//Zm interpolowane
out vec4 i_c;
out vec4 i_v;
out vec4 i_n;

out vec2 i_TexCoord0; //globalnie

void main(void) {
	i_v=vertex;
	i_c=color;
	i_n=normal;

	i_TexCoord0=texCoord0;

	gl_Position=P*V*M*vertex;
}
